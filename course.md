# „Sketchnoting“
## …dafür muss man nicht gut malen oder zeichnen können.

### Was ist Sketchnoting
Das Wort Sketchnote setzt sich zusammen aus „Sketch“ (engl. Skizze, Zeichnung) und „Note“ (engl. Notiz). Sketchnotes sind demnach visuell angereicherte Notizen, meist im Kleinformat, die nicht aus reinem Text, sondern insbesondere aus Bildern und anderen graphischen Strukturen bestehen. Ganz nach dem Motto „ein Bild sagt mehr als 1000 Worte“. Der Prozess, Sketchnotes zu erstellen, nennt sich „Sketchnoting“. Dabei stehen visuelle Ausdruckstechniken im Vordergrund. Sie sind nicht chronologisch oder linear aufgebaut, sondern eher wie ein MindMap oder abhängig vom Inhalt strukturiert.
Das Grundprinzip vom Sketchnoting kann in der dualen Kodierungstheorie von Allan Paivio (1970) begründet werden. Diese geht davon aus, dass das Gehirn die Inhalte durch zwei Kanäle verarbeitet: Durch den visuellen und verbalen Kanal. Im verbalen Kanal werden die Inhalte durch Worte aufgenommen, beim visuellen Kanal sieht das Auge visuelle Darstellungen und verarbeitet diese. Die Anregung dieser beiden Sinne führt dazu, dass die Erinnerungsfähigkeit verbessert wird.
Zudem wird unterschieden in:
**Graphic Recording:** größere Aufzeichnung von Vorträgen (visuelle Dokumentation).
**Graphic Facilitation:** Interaktion mit Teilnehmenden zur Erarbeitung von Themen und Inhalten, (im Gegensatz zur reinen Protokollation werden hier gemeinsam in der Gruppe die Inhalte visuell dargestellt). Dabei gibt es einen Graphic Facilitator, der die Ergebnisse parallel zum Moderator visuell darstellt und für alle festhält.

### Zu welchem Zweck 
Sketchnotes dienen dazu, die Inhalte auf eine andere, als die herkömmliche Art und Weise, festzuhalten und die Inhalte anders darzustellen, damit man sich diese besser einprägen kann. Mit Sketchnotes kann man sich über ein Thema austauschen, Inhalte entwickeln, aber auch Wissen vernetzen, reflektieren und üben. Sketchnoting ist ein Werkzeug für die Auseinandersetzung mit Inhalten. Mit Sketchnotes können komplexe Sachverhalte oder Zusammenhänge einfach und nachvollziehbar dargestellt werden. Zudem fördert das Sketchnoting die Kreativität.
**Vorteile:** 
- können schnell, einfach und klar erfasst werden (siehe Pfannkuchen-Rezept)
- ansprechender gestaltet (Emotionen hervorgerufen)
- Ersteller hat sich intensive Gedanken bei der Erstellung gemacht und alles genau durchdacht (Verstand und Körper arbeiten zusammen)
- an visualisierte Inhalte kann man sich oftmals besser erinnern
- Individualität
- Anregung des Gehirns
- Es macht Spaß!

Sketchnotes können von Lehrenden, aber auch Lernenden eingesetzt werden.

**Einsatzgebiete:**
- 	Private Notizen
- 	Zum eigenen Lernen
- 	Öffentliche Notizen bei Veranstaltungen
- 	Flipcharts bei Workshops, Seminaren, ...
- 	Präsentationen (digital und analog)
- 	Kreativ-Sein

### Wie geht man vor 
Zur Erstellung von Sketchnotes können handelsübliche Bleistifte, Kugelschreiber, Marker oder andere Filzstifte verwendet werden. Im Zuge der Digitalisierung wurde zudem, die Möglichkeit, Sketchnotes mit digitalen Medien (z.B. auf einem Tablet) herzustellen, weiterzuentwickeln. Dies hat den Vorteil, dass unbegrenzte Ressourcen zur Verfügung stehen. Anders ist das mit Stift und Papier, welches mit der Zeit erschöpft ist. Zudem liegen die Ergebnisse mit Medien digital vor und können geteilt und vervielfacht werden. Im Gegenzug hat die Nutzung von Papier und Stift den Vorteil, dass die motorische Fähigkeit trainiert wird und überall spontan eine Notiz angefertigt werden kann, unabhängig vom digitalen Medium, dem Akku, WLAN oder anderen technischen Voraussetzungen. 
Zu Beginn braucht man nicht mehr als ein Blatt Papier und einen Stift. Ein dickerer Filzstift oder Edding hat den Vorteil, plakativer zeichnen zu können. Durch den Einsatz von farbigen Stiften, können konkrete Inhalte in den Vordergrund gesetzt und besonders hervorgehoben werden.
Um Sketchnotes zu erstellen, muss man kein begabter Maler oder Zeichner sein. Es geht nicht darum, Details perfekt darzustellen, sondern darum die Inhalte und Zusammenhänge visuell festzuhalten, damit das Auge es leicht erfassen kann. Es ist hilfreich, einfache Skizzen anzufertigen und bestimmte Symbole zu nutzen. Dazu werden im Internet zahlreiche Tutorials und Hilfen angeboten, die einem Einsteiger erste Tipps geben können (z.B. Link von Timo, Sketchnoting.net).
 
Alles was gezeichnet werden könnte, besteht aus diesen fünf Grundelementen (vgl. Rohe, 2014, S. 16):
![5 Grundelemente](https://gitlab.com/Mastecker/sketchnoting/raw/master/pics/1.png)
### Sketchnoting Software
- Paper (Notizbuchstil, Listen erstellen, Zusammenhänge darstellen)
- Adobe Ideas (kostenlos, Ebenen ähnlich wie bei Photoshop, Gimp, etc. – aber momentan nicht auf dem deutschen Markt erhältlich)
- Procreate (auch mit Ebenen, aber pixelbasiert, nicht beliebig skalierbar, auf allen Ebenen kann man Fotos einfügen und ggf. abmalen, als Film exportierbar, Werkzeugspitzen selbst erstellen und verändern, speichern und verschicken, auch Bilder können verschickt werden (kostenpflichtig)
- Concepts: Skizzieren, entwerfen und illustrieren (Einsteiger-Version kostenlos, Pro-Paket 4,49€ mit vielen zusätzlichen Paketen, Figuren, Tools, etc.).

#### Beispiele
Podcast zu Sketchnoting 
(http://kulturkapital.org/kk010-sketchnotes/#t=0:18.886)

Veröffentlichung von Sketchnotes 
(http://www.flickr.com/groups/sketchnotes/ und http://sketchnotearmy.com)

Frau Prof. Theis-Bröhl von der HS Bremerhaven hat ihr Vorlesungsskript damit erstellt: (http://sketchnotearmy.com/blog/2017/1/16/sketchnoted-presentation-for-the-physics-show-katharina-thei.html)

Webseite zur täglichen Sketchnote Übung: (https://quickdraw.withgoogle.com/#)


##### Quellen
- Appelt, Ralf (2013). Lehren und Lernen mit Sketchnotes: http://sketchnotes.de/wp-content/uploads/2013/04/pb21_eBook_Lehren-und-Lernen-Sketchnotes.pdf 
- Rohde, Mike (2014). Das Sketchnote Handbuch. Der illustrier-te Leitfaden zum Erstellen visueller Notizen. Mtip Business.
- https://sketchnoting.net/sketchnoting-macht-kreativ/
- http://sketchnotes.de/about/#sketchnotes
- http://andrea-bruecken.de/2017/01/03/sketchnotes-im-wissentschaftlichen-kontext/

